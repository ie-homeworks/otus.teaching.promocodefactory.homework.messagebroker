﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core;
using Otus.Teaching.Pcf.Messages;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    internal class PartnerManagerAppliedPromocodeConsumer : IConsumer<PartnerManagerAppliedPromocodeMessage>
    {
        private readonly EmployeeService _employeeService;

        public PartnerManagerAppliedPromocodeConsumer(EmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<PartnerManagerAppliedPromocodeMessage> context)
        {
            await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}