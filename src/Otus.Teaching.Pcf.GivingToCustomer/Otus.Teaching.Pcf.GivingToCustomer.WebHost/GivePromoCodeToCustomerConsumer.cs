﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.Messages;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    internal class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerMessage>
    {
        private readonly PromoCodeService _promoCodeService;

        public GivePromoCodeToCustomerConsumer(PromoCodeService promoCodeService)
        {
            _promoCodeService = promoCodeService; 
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerMessage> context)
        {
            var message = context.Message;
            
            var promoCode = new PromoCode()
            {
                Id = message.PromoCodeId,
                PartnerId = message.PartnerId,
                Code = message.PromoCode,
                ServiceInfo = message.ServiceInfo,
                BeginDate = DateTime.Parse(message.BeginDate),
                EndDate = DateTime.Parse(message.EndDate),
                PreferenceId = message.PreferenceId,
            };

            _ = await _promoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(promoCode);
        }
    }
}